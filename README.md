[![pipeline status](https://gitlab.com/novanova/tgm_opencv_ocr/badges/master/pipeline.svg)](https://gitlab.com/novanova/tgm_opencv_ocr/-/commits/master)
[![coverage report](https://gitlab.com/novanova/tgm_opencv_ocr/badges/master/coverage.svg)](https://gitlab.com/novanova/tgm_opencv_ocr/-/commits/master)

Ce programme est destiné a interpreté le flux video des jeux TGM1/TAP. Il permet d'analyser chaque image et d'extraire toute information utile sous une forme textuelle.
```mermaid
graph LR
A[Running Game] --> B[Capture Card] --> C(this app) --> D(your app)
```
pour le moment les fonctionnalités sont limités à :
* seulement TGM1 en single mode (TAP n'est pas encore supporté)
*  les informations suivantes sont extraits a chaque images :
    * score
    * level / section
    * points / next grade at
    * timer

ces informations sont simplement envoyés via zmq pour être traité par n'importe quel autre programme dans le "pipeline".

grace a cette brique logicielle les possibilités qui s'offre a vous  : 
    * enregistrer des statistiques de jeu personnelles
    * créer un serveur de synchronisation pour des tournois masters (via discord ?)
    * obtenir un feedback en temps réel


Status actuel
* obtenir rapidement une MVP qui permette d'afficher en temps réel les temps de section (au détriment des tests de services :-/)

TODO : 
* mieux tester
* permettre la prise en charge de carte d'acquisition de milieu de game (custom aspect ratio)
* analyser le contenu de la zone de jeu

comment l'utiliser : 

```python 
python.exe manager.py --help
```
![demo](demo.png "demo")