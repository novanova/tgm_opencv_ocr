# pylint: disable=C0114

from setuptools import setup, find_packages

setup(name="tgm_ocr-JAGO",
      packages=find_packages(),
      version="0.0.1",
      author="JAGO",
      author_email="novanova@gmail.com",
      description="tetris the grandmaster game video data extractor",
      long_description="tetris the grandmaster game video data extractor",
      url="https://gitlab.com/novanova/tgm_opencv_ocr",
      python_requires='>=3.6'
      )
