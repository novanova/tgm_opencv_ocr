"""
test suite for the video capture class
"""
import os
import cv2
from unittest.mock import patch, Mock
from tgm_ocr import VideoCaptureService

SAMPLES = {"0G": cv2.imread(os.path.join("sample_data", "section300.bmp")),
           "20G": cv2.imread(os.path.join("sample_data", "section900.bmp")),
           "raw": cv2.imread(os.path.join("sample_data", "raw_capture.bmp"))
           }


def sample_image(key):
    return SAMPLES[key]


@patch('cv2.flip')
@patch('cv2.VideoCapture')
def test_i_can_get_a_flipped_a_frame(test_video_capture_class, cv2_mock):
    test_video_capture_class.return_value = test_video_capture_class
    test_video_capture_class.isOpened.return_value = True
    image = sample_image("raw")
    test_video_capture_class.read.return_value = (True, image)

    config = {"horizontal_flip": True}
    video_capture_service = VideoCaptureService(config)
    result = video_capture_service.get_frame()
    cv2_mock.assert_called_with(image, 0)


def capture_object_mock():
    video_capture_mock = Mock()
    video_capture_mock.isOpened = True


@patch('cv2.VideoCapture')
def test_i_can_get_a_get_a_cropped_frame(test_video_capture_class):
    test_video_capture_class.return_value = test_video_capture_class
    test_video_capture_class.isOpened.return_value = True

    test_video_capture_class.read.return_value = (True, sample_image("raw"))
    config = {'tgm': {
        'frame_crop_left': 1,
        'frame_crop_top': 6,
        'frame_crop_right': 15,
        'frame_crop_bottom': 6
        }
    }
    video_capture_service = VideoCaptureService(config)
    result = video_capture_service.get_frame()
    height, width, _ = result.shape
    assert width == 320
    assert height == 240
    # assert test_video_capture_class.isOpened.called
    # assert video_capture_service.vid.isOpened.called
    # assert test_video_capture_class == video_capture_service.vid
    # result = video_capture_service.get_frame()
    # assert result == sample_image
