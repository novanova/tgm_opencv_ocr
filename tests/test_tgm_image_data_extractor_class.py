# pylint: disable=redefined-outer-name

"""
test suite for the tgm image data extractor class
"""
import os
import cv2
import pytest

from tgm_ocr.utils.enum import DigitGroup
from tgm_ocr.tgm_image_data_extractor import TgmImageDataExtractor
from tgm_ocr.utils.enum import FontColor, FontSize


@pytest.fixture(scope="module")
def tgm_ocr():
    """ provide an instance of the tested class"""
    return TgmImageDataExtractor()


@pytest.fixture(scope="module")
def sample_image(request):
    """ provide sample capture images for the tests """
    images = {"0G": cv2.imread(os.path.join("sample_data", "section300.bmp")),
              "20G": cv2.imread(os.path.join("sample_data", "section900.bmp")),
              "raw": cv2.imread(os.path.join("sample_data", "raw_capture.bmp"))
              }
    return images[request.param]


@pytest.mark.parametrize('sample_image', [pytest.param('raw', id='raw image from capture card')],
                         indirect=['sample_image'])
def test_i_can_update_a_frame_image(tgm_ocr, sample_image):
    tgm_ocr.update_frame_to_process(sample_image)
    assert tgm_ocr._frame is not None


digit_group_data = [
    pytest.param("20G", DigitGroup.GRADE, True, "??????", id="grade"),
    pytest.param("20G", DigitGroup.POINTS, True, "170156", id="points"),
    pytest.param("20G", DigitGroup.LEVEL, True, "948", id="level"),
    pytest.param("20G", DigitGroup.SECTION, True, "999", id="section"),
    pytest.param("20G", DigitGroup.TIMER, True, "085751", id="timer")
]


@pytest.mark.parametrize("sample_image, digit_group, gold_font, expected",
                         digit_group_data, indirect=["sample_image"])
def test_i_can_read_a_digit_group(tgm_ocr, sample_image, digit_group, gold_font, expected):
    """ I can read small digit group from a tgm capture
    """
    tgm_ocr.update_frame_to_process(sample_image)
    result = tgm_ocr.read_digits_group(digit_group, gold_font)
    assert result == expected


digits_data = [
    pytest.param("0G", {'height': 9, 'width': 7, 'size': FontSize.TGM_SMALL}, {'x': 85, 'y': 182},
                 FontColor.TGM_WHITE, "3", id="small digit"),
    pytest.param("20G", {'height': 13, 'width': 9, 'size': FontSize.TGM_LARGE}, {'x': 159, 'y': 213},
                 FontColor.TGM_GOLD, "7", id="large digit"),
]


@pytest.mark.parametrize("sample_image, aspects, positions, gold_font, expected",
                         digits_data, indirect=["sample_image"])
# pylint: disable=too-many-arguments
def test_i_can_read_a_digit(tgm_ocr, sample_image, aspects, positions, gold_font, expected):
    """  I can read large white digit in a screen capture
    """
    tgm_ocr.update_frame_to_process(sample_image)
    result = tgm_ocr.read_digit(aspects, positions, gold_font)
    assert result == expected
