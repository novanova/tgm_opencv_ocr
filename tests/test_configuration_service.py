# pylint : disable=C0114
import os
import pytest
from tgm_ocr.configuration_service import ConfigurationService

CONFIG_FILE_PATH = os.path.join("tests", "config.ini")
EMPTY_CONFIG_FILE_PATH = os.path.join("tests", "empty_config.ini")


@pytest.mark.parametrize('arg, expected',
                         [pytest.param('height', None, id='height'),
                          pytest.param('width', None, id='width'),
                          pytest.param('device', 0, id='capture device'),
                          pytest.param('flip', False, id='frame flip')
                          ])
def test_there_are_fallback_values_for_missing_arguments_in_command_line_and_config_file(tmp_path, arg, expected):
    empty_config_file = create_config_file_with_empty_default_section(tmp_path)
    args = []
    configuration_service = ConfigurationService(args, empty_config_file)
    settings = configuration_service.get_config()
    assert settings[arg] == expected


@pytest.mark.parametrize('arg, expected',
                         [pytest.param('height', 42, id='height'),
                          pytest.param('width', 42, id='width'),
                          pytest.param('device', 1, id='capture device'),
                          pytest.param('flip', True, id='horizontal frame flip')
                          ])
def test_config_file_arguments_overrides_fallback_values(arg, expected):
    configuration_service = ConfigurationService(config_file_name=CONFIG_FILE_PATH)
    settings = configuration_service.get_config()
    assert settings[arg] == expected


@pytest.mark.parametrize('arg, expected',
                         [pytest.param("height", 149, id='height'),
                          pytest.param('width', 143, id='width'),
                          pytest.param('device', 18, id='capture device'),
                          pytest.param('flip', False, id='horizontal frame flip')
                          ])
def test_console_line_arguments_overrides_both_fallback_and_config_file(arg, expected):
    args = ["--" + arg + "=" + str(expected)]
    configuration_service = ConfigurationService(args, CONFIG_FILE_PATH)
    settings = configuration_service.get_config()
    assert settings[arg] == expected


def test_a_default_configuration_file_is_created_if_not_exist(tmp_path):
    """ je vérifie que la création d'un fichier par défaut fonctionne"""

    file = tmp_path / "test_config.ini"
    assert not file.is_file()
    configuration_service = ConfigurationService(config_file_name=file)
    assert len(file.read_text()) > 0


def create_config_file_with_empty_default_section(tmp):
    tmp_empty_config_file = tmp / "empty_config.ini"
    assert not tmp_empty_config_file.is_file()
    with open(tmp_empty_config_file, 'w') as fp:
        fp.write('\n[DEFAULT]\ndummy=1')

    t = tmp_empty_config_file.read_text()
    print(t)
    return tmp_empty_config_file
