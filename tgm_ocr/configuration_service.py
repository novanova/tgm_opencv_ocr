# pylint : disable=C0114
import sys
import argparse
from configparser import ConfigParser


class ConfigurationService:
    """ This class handle the program's settings parsing
        coming from the config.txt file and/or the command lines arguments
    """

    def __init__(self, args=None):
        config_file_name = "" + args[0]
        if config_file_name.endswith(".ini"):
            args = args[1:]
        else:
            config_file_name = "config_of_777.ini"
        print(config_file_name)
        
        self._config_file = self.parse_config_file(config_file_name)
        parser = self.create_parser()

        t = parser.parse_args(args)
        self._config = t.__dict__
        self.append_file_only_options_to_config()

    def get_config(self):
        """ retrieve the settings
        """
        return self._config

    def create_parser(self):
        """ the command line parser will have priority over the config file
            if a property is not found from the command line,
            and the config file then a fallback value is declared.
        """
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()

        default_device = self._config_file.get(
            'DEFAULT', 'capture_device', fallback=0)
        group.add_argument(
            "-d", "--device",
            help="the device index to capture from (default=0)",
            type=int,
            default=default_device)
        group.add_argument(
            "-f", "--file",
            help="the file to play",
            default=None)
        parser.add_argument(
            "--debug",
            help="display capture video",
            action="store_true")
        default_frame_height = self._config_file.get(
            'DEFAULT', 'frame_height', fallback=None)
        parser.add_argument(
            "--height",
            help="define the capture property height of each frame",
            type=int,
            default=default_frame_height)

        default_frame_width = self._config_file.get(
            'DEFAULT', 'frame_width', fallback=None)
        parser.add_argument(
            "--width",
            help="define the capture property width of each frame",
            type=int,
            default=default_frame_width)

        frame_flip = self._config_file.get(
            'DEFAULT', 'horizontal_flip', fallback=False)
        parser.add_argument(
            "--hflip",
            help="flip horizontally each frame",
            type=self.boolean_string,
            default=frame_flip)
        
        default_screen_mode = self._config_file.get(
            'TGM', 'screen_mode', fallback = 0)
        parser.add_argument(
            "-s", "--side",
            help="the TGM1 screen side 0/1/2 (0==center)",
            type=int,
            default=default_screen_mode)
        return parser

    @staticmethod
    def parse_config_file(filename):
        """ configuration file parser
        """
        config_parser = ConfigParser()
        config_parser.read(filename)
        if not config_parser.defaults():
            config_parser = ConfigurationService.create_default_config_file(filename)
        return config_parser

    @staticmethod
    def create_default_config_file(filename):
        """ write a default config_of_777.ini file to disk
        """
        config_parser = ConfigParser(allow_no_value=True)
        config_parser['DEFAULT'] = {
            '; generally if you only have 1 capture device and no webcam it''s always 0': None,
            '# otherwise you need to increment until the right one is selected': None,
            'capture_device': 0,
            '\n# some capture card might provide upside down video': None,
            'horizontal_flip': 'False'
        }

        config_parser['TGM'] = {
            '# if the frames have black borders they must be cropped': None,
            '# the values are in pixels from the indicated side': None,
            'frame_crop_left': 0,
            'frame_crop_top': 0,
            'frame_crop_right': 0,
            'frame_crop_bottom': 0,
            '\n# capture frame height and width can be enforced': None,
            '# 0 == default': None,
            'frame_width': 0,
            'frame_height': 0,
            'resize_height': 0,
            'resize_width': 0,
            '#playfield screen side': None,
            '# 0 = playfield centered': None,
            '# 1 = P1 side of screen': None,
            '# 2 = P2 side of screen': None,
            'screen_side': 1
        }

        config_parser['TAP'] = {
            '# if the frames have black borders they must be cropped': None,
            '# the values are in pixels from the indicated side': None,
            'frame_crop_left': 0,
            'frame_crop_top': 0,
            'frame_crop_right': 0,
            'frame_crop_bottom': 0,
            '\n# capture frame height and width can be enforced': None,
            '# 0 == default': None,
            'frame_width': 0,
            'frame_height': 0,
            'resize_height': 0,
            'resize_width': 0
        }

        with open(filename, 'w') as configfile:
            config_parser.write(configfile)

        return config_parser

    def append_file_only_options_to_config(self):

        self._config["tgm"] = {}
        self._config["tgm"]["frame_crop_left"] = int(self._config_file.get('TGM', 'frame_crop_left', fallback=0))
        self._config["tgm"]["frame_crop_top"] = int(self._config_file.get('TGM', 'frame_crop_top', fallback=0))
        self._config["tgm"]["frame_crop_right"] = int(self._config_file.get('TGM', 'frame_crop_right', fallback=0))
        self._config["tgm"]["frame_crop_bottom"] = int(self._config_file.get('TGM', 'frame_crop_bottom', fallback=0))
        self._config["tgm"]["frame_width"] = int(self._config_file.get('TGM', 'frame_width', fallback=0))
        self._config["tgm"]["frame_height"] = int(self._config_file.get('TGM', 'frame_height', fallback=0))
        self._config["tgm"]["resize_width"] = int(self._config_file.get('TGM', 'resize_width', fallback=0))
        self._config["tgm"]["resize_height"] = int(self._config_file.get('TGM', 'resize_height', fallback=0))
        self._config["tap"] = {}
        self._config["tap"]["frame_crop_left"] = int(self._config_file.get('TAP', 'frame_crop_left', fallback=0))
        self._config["tap"]["frame_crop_top"] = int(self._config_file.get('TAP', 'frame_crop_top', fallback=0))
        self._config["tap"]["frame_crop_right"] = int(self._config_file.get('TAP', 'frame_crop_right', fallback=0))
        self._config["tap"]["frame_crop_bottom"] = int(self._config_file.get('TAP', 'frame_crop_bottom', fallback=0))
        self._config["tap"]["frame_width"] = int(self._config_file.get('TAP', 'frame_width', fallback=0))
        self._config["tap"]["frame_height"] = int(self._config_file.get('TAP', 'frame_height', fallback=0))
        self._config["tap"]["resize_width"] = int(self._config_file.get('TAP', 'resize_width', fallback=0))
        self._config["tap"]["resize_height"] = int(self._config_file.get('TAP', 'resize_height', fallback=0))

    def boolean_string(self, s):
        if s not in {'False', 'True'}:
            raise ValueError('Not a valid boolean string')
        return s == 'True'
