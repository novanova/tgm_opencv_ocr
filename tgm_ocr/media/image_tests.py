import cv2

game = cv2.imread("0000.png")
dark_bg_1 = cv2.imread("tgm_bg_1_dark.png")
cv2.imshow("dark", dark_bg_1)
cv2.imshow("game", game)
result = cv2.subtract(game, dark_bg_1)
cv2.imshow("result", result)
cv2.waitKey(0)