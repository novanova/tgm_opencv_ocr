# pylint: disable=C0114

import cv2
import numpy as np
from utils.static import nested_get
from utils.enum import Game, ImageEditing


class VideoCaptureService:
    """
        provide basics function for grabbing device capture frames

    """

    def __init__(self, config):
        # Open the video source
        if config is None:
            config = {}
        video_source = config.get('file')
        if video_source is None:
            video_source = config.get("device", 0)
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # set video source width and height if provided in configuration
        frame_width = config.get('width')
        if frame_width is not None:
            self.vid.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
        frame_height = config.get('height')
        if frame_height is not None:
            self.vid.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)
        self.image_flip = config.get('hflip', False)

        self.tgm_crop = self.get_editing_values(config, 'tgm', ImageEditing.CROPPING)
        self.tgm_cropping = sum(self.tgm_crop) > 0

        self.tgm_extend = self.get_editing_values(config, 'tgm', ImageEditing.EXTENDING)
        self.tgm_extending = sum(self.tgm_extend) != 0

        self.tgm_resize = (nested_get(config, ['tgm', 'resize_width'], 0),
                           nested_get(config, ['tgm', 'resize_height'], 0))

        self.tap_crop = (nested_get(config, ['tap', 'frame_crop_left'], 0),
                         nested_get(config, ['tap', 'frame_crop_top'], 0),
                         nested_get(config, ['tap', 'frame_crop_right'], 0),
                         nested_get(config, ['tap', 'frame_crop_bottom'], 0))

        self.tap_cropping = sum(self.tap_crop) > 0

        self.active_game = Game.TGM
        self.debug = config.get('debug', False)

    @staticmethod
    def get_editing_values(config, game, editing_type):
        crops = [
                nested_get(config, [game, 'frame_crop_left'], 0),
                nested_get(config, [game, 'frame_crop_top'], 0),
                nested_get(config, [game, 'frame_crop_right'], 0),
                nested_get(config, [game, 'frame_crop_bottom'], 0)
                ]

        for index, crop in enumerate(crops):
            if editing_type == ImageEditing.CROPPING:
                crops[index] = crop if crop > 0 else 0
            else:
                crops[index] = -crop if crop < 0 else 0

        return tuple(crops)

    def get_frame(self):
        if not self.vid.isOpened():
            return None
        ret, frame = self.vid.read()
        if not ret:
            return None
        if self.active_game == Game.TGM:
            if self.tgm_resize:
                frame = self.resize_frame(frame, self.tgm_resize)
            if self.tgm_extending:
                frame = self.extend_frame(frame, self.tgm_extend)
            if self.tgm_cropping:
                frame = self.crop_frame(frame, self.tgm_crop)

        else:
            if self.tap_cropping :
                frame = self.crop_frame(frame, self.tap_crop)

        if self.image_flip:
            frame = self.flip_frame(frame)

        if self.debug: #119,38, 200, 201
            cv2.imshow('capture_service output', frame)
            cv2.waitKey(1)
        return frame

    def crop_frame(self, frame, crop_values):
        l, t, r, b = crop_values
        height, width, _ = frame.shape
        return frame[b:height-t, l:width-r]

    def extend_frame(self, frame, extend_values):
        l, t, r, b = extend_values
        return cv2.copyMakeBorder(frame, t, b, l, r, cv2.BORDER_CONSTANT, (255, 255, 0))

    def resize_frame(self, frame, resize_values):
        return cv2.resize(frame, resize_values, interpolation=cv2.INTER_AREA)

    def flip_frame(self, frame):
        return cv2.flip(frame, 0)

    def set_active_game(self, game=Game.TGM):
        self.active_game = game


    # Release the video source
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()
