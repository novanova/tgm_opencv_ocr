"""
this module contains all enum of this project
"""
from enum import Enum, auto


class DigitGroup(Enum):
    """ digit group enumeration
    """
    POINTS = auto()
    GRADE = auto()
    LEVEL = auto()
    SECTION = auto()
    TIMER = auto()


class DigitType(Enum):
    """ digit type size"""
    LARGE = auto()
    SMALL = auto()


class PlayfieldPosition(Enum):
    """position of the playfield depending if the game
    is configured in SINGLE or VS mode
    """
    CENTER = auto()
    LEFT = auto()
    RIGHT = auto()


class FontSize(Enum):
    """ type of font size ingame"""
    TGM_SMALL = auto()
    TGM_LARGE = auto()


class FontColor(Enum):
    """ type of font size ingame"""
    TGM_WHITE = auto()
    TGM_GOLD = auto()
    TGM_MASK = auto()


class Game(Enum):
    """ all supported game"""
    TGM = auto()
    TAP = auto()


class Artifacts(Enum):
    """ artifact used in detection"""
    NEXT = auto()
    LEVEL = auto()
    PLAYFIELD = auto()


class ImageEditing(Enum):
    CROPPING = auto()
    EXTENDING = auto()
