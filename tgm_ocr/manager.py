"""
    this program extract human readable information in real time from the TGM series game.
    the program functionnality :
    - capture each frame from the ....
"""

import sys
import zmq
from math import trunc
import time
import cv2
from configuration_service import ConfigurationService
from video_capture_service import VideoCaptureService
from tgm_image_data_extractor import TgmImageDataExtractor

from utils.enum import DigitGroup, PlayfieldPosition


def init_zmq():
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    identity = u'TGM-JAGO'
    socket.identity = identity.encode('ascii')
    socket.setsockopt(zmq.SNDHWM, 10)
    socket.connect('tcp://localhost:5570')
    poll = zmq.Poller()
    poll.register(socket, zmq.POLLIN)
    return socket


def main():

    config = ConfigurationService(sys.argv[1:]).get_config()
    # config = ConfigurationService(sys.argv[1:]).get_config() # 777
    cap = VideoCaptureService(config)
    debug = config.get('debug', False)
    reader = TgmImageDataExtractor(debug)
    socket = init_zmq()
    frame_count = 0

    background_mask = 0
    filename = "recording/replay" + time.strftime("%Y%m%d-%H%M%S") + ".txt"
    replay_file = open(filename, "a")
    screen_side = get_screen_side(config.get('side'))
    print("screen_mode = " + str(screen_side))
    while True:
        frame = cap.get_frame()
        if frame is None:
            continue
        frame_count += 1

        reader.update_frame_to_process(frame)

        is_game_screen = reader.is_game_screen(side=screen_side)
        if is_game_screen:
            score = reader.read_digits_group(DigitGroup.POINTS, side=screen_side)
            if "?" in score:
                score = reader.read_digits_group(DigitGroup.POINTS, True, side=screen_side)
            grade = reader.read_digits_group(DigitGroup.GRADE, side=screen_side)
            if "?" in grade and grade != "??????":
                grade = reader.read_digits_group(DigitGroup.GRADE, True, side=screen_side)
            level = reader.read_digits_group(DigitGroup.LEVEL, side=screen_side)
            if "?" in level:
                level = reader.read_digits_group(DigitGroup.LEVEL, True, side=screen_side)
            timer = reader.read_digits_group(DigitGroup.TIMER, side=screen_side)

            section = reader.read_digits_group(DigitGroup.SECTION, side=screen_side)
            if "?" in section:
                section = reader.read_digits_group(DigitGroup.SECTION, True, side=screen_side)

            mask = intTryParseSection(section[:1])
            if mask != -1:
                background_mask = mask
            playfield = reader.read_playfield_content(background_mask, debug, side=screen_side)
        else:
            timer = "?"
            score = "?"
            grade = "?"
            level = "?"
            section = "?"
            playfield = ""

        if debug:
            debug_frame = reader.get_debug_frame()
            cv2.imshow("extraction_frame", debug_frame)
            cv2.waitKey(1)
        else:
            print(timer)
        attract_mode = not is_game_screen
        replay_file.write(u'%d,%s,%s,%s,%s,%s,%s,%s\n' % (frame_count, attract_mode, timer, score, grade, level,
                                                          section, playfield))
        try:
            # very basic for testing purpose
            socket.send_string(
                u'%d,%s,%s,%s,%s,%s,%s,%s' % (frame_count, attract_mode, timer, score, grade, level, section,
                                              playfield),
                flags=zmq.NOBLOCK)

        except zmq.error.Again:
            pass

def get_screen_side(side):
    match side:
        case 0:
            return PlayfieldPosition.CENTER
        case 1:
            return PlayfieldPosition.LEFT
        case 2:
            return PlayfieldPosition.RIGHT
        case _:
           return PlayfieldPosition.CENTER
        



def intTryParseSection(value):
    try:
        return int(value) - 1
    except ValueError:
        return -1


if __name__ == "__main__":
    main()
