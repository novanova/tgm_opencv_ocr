# pylint : disable=C0114
import cv2
import math

import numpy as np
from utils.enum import DigitGroup, FontColor, Artifacts, PlayfieldPosition
from image_data import INGAME_DIGIT_GROUP_DATA, ARTIFACT_DATA, OCR_TEMPLATES, PLAYFIELD_INNER_ROI, BLOCK_COLORS, TXT_BG
from txt_wrapper import draw_text


class TgmImageDataExtractor:
    """
        permet d'extraires les informations utiles d'une capture d'écran :
        - les groupes de caractères numériques pour les points, levels, etc.
        - les grands caractères numériques utilisés pour le chronomètre
        - le contenu de la zone de jeu
        - la pièce suivante
        - le grade
        - etc.
    """

    def __init__(self, debug=False):
        self._templates = OCR_TEMPLATES
        self._ingame_digit_group_data = INGAME_DIGIT_GROUP_DATA
        self._artifact_data = ARTIFACT_DATA
        self._frame = None
        self._debug_frame = None
        self._pickRows = PLAYFIELD_INNER_ROI["pickRows"]
        self._pickColumns = PLAYFIELD_INNER_ROI["pickColumns"]
        self.debug = debug

    def update_frame_to_process(self, img):
        """ update a new frame for applying any ocr functions"""
        self._frame = img
        if self.debug:
            self._debug_frame = img.copy()

    def is_game_screen(self, side=PlayfieldPosition.CENTER, gold_font=False):
        """
            permet de lire si l'on est sur l'écran de jeu ou pas (pour le moment seul la zone de jeu centrale
            est prise en charge
        """
        font_color = [FontColor.TGM_WHITE, FontColor.TGM_GOLD][gold_font]

        level = self.read_artifact(Artifacts.LEVEL, side, font_color)
        if level > 0.90:
            return True
        next_txt = self.read_artifact(Artifacts.NEXT, side, font_color)
        return next_txt > 0.90

    def read_artifact(self, artifact, side, font_color):
        data = self._artifact_data[artifact]
        y_pos = data["positions"][side]['y']
        x_pos = data["positions"][side]['x']
        height = data['height']
        width = data['width']
        roi = self._frame[y_pos:y_pos + height, x_pos:x_pos + width]
        template = self._templates[artifact][font_color]
        mask_template = self._templates[artifact][FontColor.TGM_MASK]
        res = cv2.matchTemplate(
            roi,
            template,
            cv2.TM_CCORR_NORMED,
            template,
            mask=mask_template)  # TM_CCORR_NORMED

        if self.debug:
            start_point = (x_pos-1, y_pos-1)
            end_point = (x_pos + width, y_pos + height)
            color = (0, 0, 255)
            thickness = -1
            cv2.rectangle(self._debug_frame, start_point, end_point, color, 1)

        return res[0, 0]

    def read_digits_group(self, digit_group=DigitGroup.POINTS, gold_font=False, side=PlayfieldPosition.CENTER):
        """
            Permet de lire un groupe de petit caractère numérique de type blanc ou doré
        """
        result = ''
        digits_group_data = self._ingame_digit_group_data[digit_group]
        font_color = [FontColor.TGM_WHITE, FontColor.TGM_GOLD][gold_font]
        for position in digits_group_data['positions'][side]:
            result += self.read_digit(digits_group_data, position, font_color)
        return result

    def read_digit(self, aspects, coordinates, font_color):
        """ permet de lire si un petit caractère numérique se trouve
            aux positions x et y , dans une capture d'écran.
        """
        y_pos = coordinates['y']
        x_pos = coordinates['x']
        height = aspects['height']
        width = aspects['width']
        roi = self._frame[y_pos:y_pos + height, x_pos:x_pos + width]
        digits_templates = self._templates[aspects['size']][font_color]
        mask_templates = self._templates[aspects['size']][FontColor.TGM_MASK]

        max_result = 0

        for key in digits_templates.keys():
            res = cv2.matchTemplate(
                roi,
                digits_templates[key],
                cv2.TM_CCORR_NORMED,
                digits_templates[key],
                mask=mask_templates[key])  # TM_CCORR_NORMED
            # direct numpy access instead of  cv2.minMaxLoc(res) since the result is only one entry
            max_value = res[0, 0]
            if max_value > max_result:
                max_result = max_value
                result = key

        if self.debug:
            start_point = (x_pos-1, y_pos-1)
            end_point = (x_pos + width, y_pos + height)
            color = (255, 255, 0)
            thickness = -1
            cv2.rectangle(self._debug_frame, start_point, end_point, color, 1)

        return result if max_result >= 0.90 else " "

    def read_playfield_content(self, section, debug=False, side=PlayfieldPosition.CENTER):  # this function is a fucking mess
        piece = [" ", "I", "i", "1", "L", "l", "2", "J", "j", "3",
                 "T", "t", "4", "Z", "z", "5", "S", "s", "6",
                 "O", "o", "7", "X"]
        x_pos = self._artifact_data[Artifacts.PLAYFIELD][side]['x']
        y_pos = ARTIFACT_DATA[Artifacts.PLAYFIELD][side]['y']
        height = ARTIFACT_DATA[Artifacts.PLAYFIELD][side]['height']
        width = ARTIFACT_DATA[Artifacts.PLAYFIELD][side]['width']

        inner_playfield = self._frame[y_pos:y_pos + height, x_pos:x_pos + width]
        linear_light_mask = self._templates[Artifacts.PLAYFIELD][side][section]
        cleaned_inner_playfield = cv2.subtract(inner_playfield, linear_light_mask)

        inner_playfield = self.get_inner_block_roi_pixels(inner_playfield)
        cleaned_inner_playfield = self.get_inner_block_roi_pixels(cleaned_inner_playfield)
        if debug:
            cv2.imshow("cleaned_inner_playfield", cleaned_inner_playfield)
            blank_image = np.zeros((160, 80, 3), dtype=np.uint8)

        cleaned_mean = self.get_image_mean(cleaned_inner_playfield)
        original_mean = self.get_image_mean(inner_playfield)
        result = ""

        for incr in range(0, 200):
            pixel_mean = sum(cleaned_mean[0][incr]) / 3
            if pixel_mean < 10:
                result += " "
                pixel_color = (0, 0, 0)
            else:
                pixel_color = tuple(original_mean[0][incr])
                l = piece[self.calculate_closest_color_match(original_mean[0][incr])]
                result += l
            if debug:
                x_pos = (incr * 8) % 80
                y_pos = (math.floor((incr * 8) / 80) * 8)
                pt1 = (x_pos, y_pos)
                pt2 = (x_pos + 8, y_pos + 8)
                cv2.rectangle(blank_image, pt1, pt2, pixel_color, -1)

        if debug:
            cv2.imshow("redraw", blank_image)

        return result

    def get_inner_block_roi_pixels(self, img):
        tmp = img[self._pickRows, :, :]
        tmp = tmp[:, self._pickColumns, :]  # keep only the inner pixels of each blocs
        return tmp

    def get_debug_frame(self):
        return self._debug_frame
    @staticmethod
    def get_image_mean(img):
        m, n = 4, 4
        rowlen = 40
        reshaped_img = img.reshape(-1, m, rowlen // n, n, 3) \
            .swapaxes(1, 2) \
            .reshape(-1, m * n, 3) \
            .swapaxes(1, 2)

        result = np.mean(reshaped_img, 2, dtype=np.uint16).astype(np.uint8).reshape(1, 200, 3).tolist()
        return result

    @staticmethod
    def calculate_closest_color_match(color):
        temp = np.subtract(BLOCK_COLORS, color)
        temp = np.absolute(temp)
        temp = np.sum(temp, axis=2)
        return np.argmin(temp)
