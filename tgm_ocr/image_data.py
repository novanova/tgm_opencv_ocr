"""
    this module contains all static image data from games :
    - digits positions and sizes
    - templates images for OCR
    - etc.
"""
import os
import cv2
import numpy as np
from utils.enum import DigitGroup, FontSize, FontColor, Artifacts, PlayfieldPosition

TEMPLATE_IMAGE = cv2.imread(os.path.join("media", "OCR_template.png"))
MASK_TEMPLATE_IMAGE = cv2.imread(os.path.join("media", "OCR_mask_template.png"))
BG_DARK_IMAGES = [cv2.imread(os.path.join("media", "tgm_bg_1_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_2_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_3_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_4_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_5_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_6_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_7_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_8_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_9_dark.png")),
                  cv2.imread(os.path.join("media", "tgm_bg_10_dark.png"))]

TXT_BG = cv2.imread(os.path.join("media", "txt_bg.png"))

BLOCK_COLORS = np.array([ # as BGR instead of RGB
    0, 0, 0,  # black
    16, 59, 254, 4, 24, 121, 1, 13, 71,  # red
    0, 144, 254, 0, 62, 115, 0, 36, 67 ,  # orange
    251, 106, 1, 114, 44, 2, 66, 25, 2,  # blue
    241, 219, 0, 103, 92, 2, 59, 54, 2,  # ice blue
    0, 225, 25, 0, 99, 11, 0, 57, 6,  # green
    225, 20, 225, 98, 7, 98, 55, 4, 58,  # purple
    0, 204, 222, 0, 88, 95, 0, 51, 55,  # yellow
    247, 241, 212  # grey
], dtype=np.uint8).reshape(1, 23, 3)

ARTIFACT_DATA = {
    Artifacts.NEXT: {
        'height': 6,
        'width': 19,
        'positions': {
            PlayfieldPosition.CENTER: {
                'x': 113,
                'y': 24
            },
            PlayfieldPosition.LEFT: {
                'x': 12,
                'y': 24
            },
            PlayfieldPosition.RIGHT: {
                'x': 216,
                'y': 24
            }
        }
    },
    Artifacts.LEVEL: {
        'height': 6,
        'width': 20,
        'positions': {
            PlayfieldPosition.CENTER: {
                'x': 89,
                'y': 171
            },
            PlayfieldPosition.LEFT: {
                'x': 108,
                'y': 171
            },
            PlayfieldPosition.RIGHT: {
                'x': 192,
                'y': 171
            }
        }
    },
    Artifacts.PLAYFIELD: {
        PlayfieldPosition.CENTER: {
            'x': 119,
            'y': 38,
            'height': 163,
            'width': 81,
        },
        PlayfieldPosition.LEFT: {
            'x': 16,
            'y': 38,
            'height': 163,
            'width': 82,
        },
        PlayfieldPosition.RIGHT: {
            'x': 221,
            'y': 38,
            'height': 163,
            'width': 82,
        }
    }
}

INGAME_DIGIT_GROUP_DATA = {
    DigitGroup.GRADE: {
        'height': 9,
        'width': 7,
        'size': FontSize.TGM_SMALL,
        'positions': {
            PlayfieldPosition.CENTER: [
                {'index': 1, 'x': 62, 'y': 77},
                {'index': 2, 'x': 70, 'y': 77},
                {'index': 3, 'x': 78, 'y': 77},
                {'index': 4, 'x': 86, 'y': 77},
                {'index': 5, 'x': 94, 'y': 77},
                {'index': 6, 'x': 102, 'y': 77}
            ],
            PlayfieldPosition.LEFT: [
                {'index': 1, 'x': 108, 'y': 77},
                {'index': 2, 'x': 116, 'y': 77},
                {'index': 3, 'x': 124, 'y': 77},
                {'index': 4, 'x': 132, 'y': 77},
                {'index': 5, 'x': 140, 'y': 77},
                {'index': 6, 'x': 148, 'y': 77}
            ],
            PlayfieldPosition.RIGHT: [
                {'index': 1, 'x': 165, 'y': 77},
                {'index': 2, 'x': 173, 'y': 77},
                {'index': 3, 'x': 181, 'y': 77},
                {'index': 4, 'x': 189, 'y': 77},
                {'index': 5, 'x': 197, 'y': 77},
                {'index': 6, 'x': 205, 'y': 77}
            ],
        }
    },
    DigitGroup.POINTS: {
        'height': 9,
        'width': 7,
        'size': FontSize.TGM_SMALL,
        'positions': {
            PlayfieldPosition.CENTER: [
                {'index': 1, 'x': 62, 'y': 143},
                {'index': 2, 'x': 70, 'y': 143},
                {'index': 3, 'x': 78, 'y': 143},
                {'index': 4, 'x': 86, 'y': 143},
                {'index': 5, 'x': 94, 'y': 143},
                {'index': 6, 'x': 102, 'y': 143}
            ],
            PlayfieldPosition.LEFT: [
                {'index': 1, 'x': 108, 'y': 143},
                {'index': 2, 'x': 116, 'y': 143},
                {'index': 3, 'x': 124, 'y': 143},
                {'index': 4, 'x': 132, 'y': 143},
                {'index': 5, 'x': 140, 'y': 143},
                {'index': 6, 'x': 148, 'y': 143}
            ],
            PlayfieldPosition.RIGHT: [
                {'index': 1, 'x': 165, 'y': 143},
                {'index': 2, 'x': 173, 'y': 143},
                {'index': 3, 'x': 181, 'y': 143},
                {'index': 4, 'x': 189, 'y': 143},
                {'index': 5, 'x': 197, 'y': 143},
                {'index': 6, 'x': 205, 'y': 143}
            ],
        }
    },
    DigitGroup.LEVEL: {
        'height': 9,
        'width': 7,
        'size': FontSize.TGM_SMALL,
        'positions': {
            PlayfieldPosition.CENTER: [
                {'index': 1, 'x': 85, 'y': 182},
                {'index': 2, 'x': 93, 'y': 182},
                {'index': 3, 'x': 101, 'y': 182}
            ],
            PlayfieldPosition.LEFT: [
                {'index': 1, 'x': 107, 'y': 182},
                {'index': 2, 'x': 115, 'y': 182},
                {'index': 3, 'x': 123, 'y': 182}
            ],
            PlayfieldPosition.RIGHT: [
                {'index': 1, 'x': 188, 'y': 182},
                {'index': 2, 'x': 196, 'y': 182},
                {'index': 3, 'x': 204, 'y': 182}
            ],
        }
    },
    DigitGroup.SECTION: {
        'height': 9,
        'width': 7,
        'size': FontSize.TGM_SMALL,
        'positions': {
            PlayfieldPosition.CENTER: [
                {'index': 1, 'x': 85, 'y': 198},
                {'index': 2, 'x': 93, 'y': 198},
                {'index': 3, 'x': 101, 'y': 198}
            ],
            PlayfieldPosition.LEFT: [
                {'index': 1, 'x': 107, 'y': 198},
                {'index': 2, 'x': 115, 'y': 198},
                {'index': 3, 'x': 123, 'y': 198}
            ],
            PlayfieldPosition.RIGHT: [
                {'index': 1, 'x': 188, 'y': 198},
                {'index': 2, 'x': 196, 'y': 198},
                {'index': 3, 'x': 204, 'y': 198}
            ],
        }
    },
    DigitGroup.TIMER: {
        'height': 13,
        'width': 9,
        'size': FontSize.TGM_LARGE,
        'positions': {
            PlayfieldPosition.CENTER: [
                {'index': 1, 'x': 121, 'y': 213},
                {'index': 2, 'x': 131, 'y': 213},
                {'index': 3, 'x': 149, 'y': 213},
                {'index': 4, 'x': 159, 'y': 213},
                {'index': 5, 'x': 177, 'y': 213},
                {'index': 6, 'x': 187, 'y': 213}
            ],
            PlayfieldPosition.LEFT: [
                {'index': 1, 'x': 18, 'y': 212},
                {'index': 2, 'x': 28, 'y': 212},
                {'index': 3, 'x': 46, 'y': 212},
                {'index': 4, 'x': 56, 'y': 212},
                {'index': 5, 'x': 74, 'y': 212},
                {'index': 6, 'x': 84, 'y': 212}
            ],
            PlayfieldPosition.RIGHT: [
                {'index': 1, 'x': 223, 'y': 212},
                {'index': 2, 'x': 233, 'y': 212},
                {'index': 3, 'x': 251, 'y': 212},
                {'index': 4, 'x': 261, 'y': 212},
                {'index': 5, 'x': 279, 'y': 212},
                {'index': 6, 'x': 289, 'y': 212}
            ],
        }
    },
}


def define_small_digits_templates(templates):
    """
        fonction utilitaire à l'initialisation
        permettant de définir les données nécessaires à
        la lecture des petits caractères numériques
    """
    template_tgm_white_small_digits = {}
    template_tgm_mask_small_digits = {}
    template_tgm_gold_small_digits = {}

    digit_height = 9
    digit_width = 7
    spacer = 1
    gold_digit_y_offset_position = -10
    template_digits_group_position_x = 0
    template_digits_group_position_y = 20
    digit_keys = ["0", "1", "2", "3",
                  "4", "5", "6", "7", "8", "9", "?"]

    for num, key in enumerate(digit_keys):
        x_pos = template_digits_group_position_x + \
                ((spacer + digit_width) * num)
        y_pos = template_digits_group_position_y

        template_tgm_white_small_digits[key] = \
            TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]
        template_tgm_mask_small_digits[key] = \
            MASK_TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]
        y_pos += gold_digit_y_offset_position
        template_tgm_gold_small_digits[key] = \
            TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]

    templates[FontSize.TGM_SMALL][FontColor.TGM_WHITE] = template_tgm_white_small_digits
    templates[FontSize.TGM_SMALL][FontColor.TGM_MASK] = template_tgm_mask_small_digits
    templates[FontSize.TGM_SMALL][FontColor.TGM_GOLD] = template_tgm_gold_small_digits


def define_large_digits_templates(templates):
    """
        fonction utilitaire à l'initialisation
        permettant de définir les données nécessaires à
        la lecture des grands caractères numériques
        principalement utilisés dans le chronomètre.
    """
    template_tgm_white_large_digits = {}
    template_tgm_mask_large_digits = {}
    template_tgm_gold_large_digits = {}

    digit_height = 13
    digit_width = 9
    spacer = 1
    gold_digit_y_offset_position = -14
    template_digits_group_position_x = 0
    template_digits_group_position_y = 44
    digit_keys = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    for num, key in enumerate(digit_keys):
        x_pos = template_digits_group_position_x + \
                ((spacer + digit_width) * num)
        y_pos = template_digits_group_position_y
        template_tgm_white_large_digits[key] = \
            TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]
        template_tgm_mask_large_digits[key] = \
            MASK_TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]
        y_pos += gold_digit_y_offset_position
        template_tgm_gold_large_digits[key] = \
            TEMPLATE_IMAGE[y_pos:y_pos + digit_height, x_pos:x_pos + digit_width]

    templates[FontSize.TGM_LARGE][FontColor.TGM_WHITE] = template_tgm_white_large_digits
    templates[FontSize.TGM_LARGE][FontColor.TGM_MASK] = template_tgm_mask_large_digits
    templates[FontSize.TGM_LARGE][FontColor.TGM_GOLD] = template_tgm_gold_large_digits


def define_artifacts_templates(templates):
    templates[Artifacts.NEXT][FontColor.TGM_WHITE] = TEMPLATE_IMAGE[25:31, 152:171]
    templates[Artifacts.NEXT][FontColor.TGM_GOLD] = TEMPLATE_IMAGE[1:6, 152:171]
    templates[Artifacts.NEXT][FontColor.TGM_MASK] = MASK_TEMPLATE_IMAGE[25:31, 152:171]
    templates[Artifacts.LEVEL][FontColor.TGM_WHITE] = TEMPLATE_IMAGE[25:31, 130:150]
    templates[Artifacts.LEVEL][FontColor.TGM_GOLD] = TEMPLATE_IMAGE[1:6, 130:150]
    templates[Artifacts.LEVEL][FontColor.TGM_MASK] = MASK_TEMPLATE_IMAGE[25:31, 130:150]


def define_playfield_background_templates(templates):
    for pos in PlayfieldPosition:
        for num, img in enumerate(BG_DARK_IMAGES):
            x_pos = ARTIFACT_DATA[Artifacts.PLAYFIELD][pos]['x']
            y_pos = ARTIFACT_DATA[Artifacts.PLAYFIELD][pos]['y']
            height = ARTIFACT_DATA[Artifacts.PLAYFIELD][pos]['height']
            width = ARTIFACT_DATA[Artifacts.PLAYFIELD][pos]['width']
            roi = img[y_pos:y_pos + height, x_pos:x_pos + width]
            templates[Artifacts.PLAYFIELD][pos].append(roi)


PLAYFIELD_INNER_ROI = {"pickRows": [], "pickColumns": []}


def define_playfield_block_pixel_region_of_interest():
    # only for center position of the screen
    wider_column = 5 # only one wider column in tgm1 single mode
    wider_rows = set([5, 10, 16])
    block_size = 8
    y_pos = -block_size
    x_pos = -block_size
    for incr in range(0, 20):
        y_pos += block_size
        if incr in wider_rows:
            y_pos += 1
        PLAYFIELD_INNER_ROI["pickRows"] += [y_pos + 2, y_pos + 3, y_pos + 4, y_pos + 5]
    for incr in range(0, 10):
        x_pos += block_size
        if incr == wider_column:
            x_pos +=1
        PLAYFIELD_INNER_ROI["pickColumns"] += [x_pos + 2, x_pos + 3, x_pos + 4, x_pos + 5]
    # left position
    # right position


OCR_TEMPLATES = {
    FontSize.TGM_LARGE: {},
    FontSize.TGM_SMALL: {},
    Artifacts.NEXT: {},
    Artifacts.LEVEL: {},
    Artifacts.PLAYFIELD: {
        PlayfieldPosition.CENTER: [],
        PlayfieldPosition.LEFT: [],
        PlayfieldPosition.RIGHT: [],
    }}

define_small_digits_templates(OCR_TEMPLATES)
define_large_digits_templates(OCR_TEMPLATES)
define_artifacts_templates(OCR_TEMPLATES)
define_playfield_background_templates(OCR_TEMPLATES)
define_playfield_block_pixel_region_of_interest()
